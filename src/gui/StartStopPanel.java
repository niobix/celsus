package gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import bundles.InputBundle;

public class StartStopPanel extends JPanel implements ActionListener {
	private static final long serialVersionUID = -3637937764260222085L;
	
	private InputBundle inputBundle;
	private JButton startButton;
	private JButton stopButton;

	public StartStopPanel(InputBundle inputBundle) {
		
		this.inputBundle = inputBundle;
		
		Font font = new Font("Lucida Sans Typewriter", Font.PLAIN, 18);
		
		GridBagConstraints c = new GridBagConstraints();
		this.setLayout(new GridBagLayout());
		int gridx = 0;
		int gridy = 0;
		
		c.gridx = gridx;
		c.gridy = gridy;
		c.insets = new Insets( 10, 10, 10, 10);
		startButton = new JButton("START");
		startButton.setFont(font);
		startButton.setBackground(Color.black);
		startButton.setForeground(Color.green);
		startButton.setActionCommand("START");
		startButton.addActionListener(this);
		this.add(startButton, c);
		
		gridx++;
		
		c.gridx = gridx;
		stopButton = new JButton("STOP");
		stopButton.setFont(font);
		stopButton.setBackground(Color.black);
		stopButton.setForeground(Color.green);
		stopButton.setActionCommand("STOP");
		stopButton.addActionListener(this);
		this.add(stopButton, c);
		
	}

	@Override
	public void actionPerformed(ActionEvent a) {
		// TODO Auto-generated method stub
		
		if (a.getActionCommand().equals("START")) {
			inputBundle.setStart(true);
		} else if (a.getActionCommand().equals("STOP")) {
			inputBundle.setStart(false);
		}
		
	}
	
}
