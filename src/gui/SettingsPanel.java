package gui;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import bundles.SettingsBundle;

public class SettingsPanel extends JPanel implements ActionListener {
	private static final long serialVersionUID = -9058612846186859958L;

	SettingsBundle settingsBundle;
	
	private JRadioButton twoLink;
	private JRadioButton threeLink;
	private JRadioButton test1;
	private ButtonGroup settingGroup;
	
	SettingsPanel(SettingsBundle settingsBundle) {
		
		this.settingsBundle = settingsBundle;
		
		Font font = new Font("Lucida Sans Typewriter", Font.PLAIN, 18);
		
		settingGroup = new ButtonGroup();
		
		GridBagConstraints c = new GridBagConstraints();
		this.setLayout(new GridBagLayout());
		int gridx = 0;
		int gridy = 0;
	
		c.gridx = gridx;
		c.gridy = gridy;
		c.insets = new Insets( 10, 10, 10, 10 );
		twoLink = new JRadioButton("Build Pairs", settingsBundle.isTwoPointLink());
		twoLink.setFont(font);
		twoLink.setActionCommand("2");
		twoLink.addActionListener(this);
		settingGroup.add(twoLink);
		this.add(twoLink, c);
		
		gridy++;
		
		c.gridy = gridy;
		threeLink = new JRadioButton("Find 3 Point Links", settingsBundle.isThreePointLink());
		threeLink.setFont(font);
		threeLink.setActionCommand("3");
		threeLink.addActionListener(this);
		settingGroup.add(threeLink);
		this.add(threeLink, c);
		
		gridy++;
		
		c.gridy = gridy;
		test1 = new JRadioButton("Test 1", settingsBundle.isTest1());
		test1.setFont(font);
		test1.setActionCommand("TEST1");
		test1.addActionListener(this);
		settingGroup.add(test1);
		this.add(test1, c);
	    
	
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
		if (e.getActionCommand().equals("2")) {
			settingsBundle.setTwoPointLink(true);
		} else if (e.getActionCommand().equals("3")) {
			settingsBundle.setThreePointLink(true);
		} else if (e.getActionCommand().equals("TEST1")) {
			settingsBundle.setTest1(true);
		}
		
	}
	
}
