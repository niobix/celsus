package gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import bundles.DataBundle;

public class DataPanel extends JPanel {
	private static final long serialVersionUID = -6832799957802451812L;

	DataBundle dataBundle;
	
	JLabel totalPairsLabel;
	JTextField totalPairsText;
	
	JLabel totalThreeLinkLabel;
	JTextField totalThreeLinkText;
	
	JLabel test1Label;
	JTextField test1Text;
	
	JLabel startTimeLabel;
	JTextField startTimeText;
	
	JLabel stopTimeLabel;
	JTextField stopTimeText;
	
	public DataPanel(DataBundle dataBundle) {
		
		this.dataBundle = dataBundle;
		dataBundle.setDataPanel(this);
		
		Font font = new Font("Lucida Sans Typewriter", Font.PLAIN, 18);
		
		GridBagConstraints c = new GridBagConstraints();
		this.setLayout(new GridBagLayout());
		int gridx = 0;
		int gridy = 0;
		
		c.gridx = gridx;
		c.gridy = gridy;
		c.insets = new Insets( 10, 10, 10, 10);
		totalThreeLinkLabel = new JLabel("Total 3x Links: ");
		totalThreeLinkLabel.setFont(font);
		this.add(totalThreeLinkLabel, c);
		
		gridx++;
		
		c.gridx = gridx;
		totalThreeLinkText = new JTextField("" + dataBundle.getTotalThreeLinkCount(), 10);
		totalThreeLinkText.setFont(font);
		totalThreeLinkText.setForeground(Color.green);
		totalThreeLinkText.setBackground(Color.black);
		this.add(totalThreeLinkText, c);
		
		gridy++;
		gridx = 0;
		
		c.gridx = gridx;
		c.gridy = gridy;
		c.insets = new Insets( 10, 10, 10, 10);
		test1Label = new JLabel("Test 1: ");
		test1Label.setFont(font);
		this.add(test1Label, c);
		
		gridx++;
		
		c.gridx = gridx;
		test1Text = new JTextField("" + dataBundle.getTestInt1(), 10);
		test1Text.setFont(font);
		test1Text.setForeground(Color.green);
		test1Text.setBackground(Color.black);
		this.add(test1Text, c);
		
		gridy++;
		gridx = 0;
		
		c.gridx = gridx;
		c.gridy = gridy;
		c.insets = new Insets( 10, 10, 10, 10);
		totalPairsLabel = new JLabel("Total Pairs: ");
		totalPairsLabel.setFont(font);
		this.add(totalPairsLabel, c);
		
		gridx++;
		
		c.gridx = gridx;
		totalPairsText = new JTextField("" + dataBundle.getTotalPairCount(), 10);
		totalPairsText.setFont(font);
		totalPairsText.setForeground(Color.green);
		totalPairsText.setBackground(Color.black);
		this.add(totalPairsText, c);
		
		gridx = 0;
		gridy++;
		
		c.gridx = gridx;
		c.gridy = gridy;
		startTimeLabel = new JLabel("Start Time: ");
		startTimeLabel.setFont(font);
		this.add(startTimeLabel, c);
		
		gridx++;
		
		c.gridx = gridx;
		startTimeText = new JTextField("" + dataBundle.getStartTime(), 10);
		startTimeText.setFont(font);
		startTimeText.setForeground(Color.green);
		startTimeText.setBackground(Color.black);
		this.add(startTimeText, c);
		
		gridx = 0;
		gridy++;
		
		c.gridx = gridx;
		c.gridy = gridy;
		stopTimeLabel = new JLabel("Stop Time: ");
		stopTimeLabel.setFont(font);
		this.add(stopTimeLabel, c);
		
		gridx++;
		
		c.gridx = gridx;
		stopTimeText = new JTextField("" + dataBundle.getStopTime(), 10);
		stopTimeText.setFont(font);
		stopTimeText.setForeground(Color.green);
		stopTimeText.setBackground(Color.black);
		this.add(stopTimeText, c);
		
	}
	
	public void setTotalPairCount(int count) {
		totalPairsText.setText("" + count);
	}
	
	public void setTotalThreeLinkCount(int count) {
		totalThreeLinkText.setText("" + count);
	}
	
	public void setStartTime(String time) {
		startTimeText.setText(time);
	}
	
	public void setStopTime(String time) {
		stopTimeText.setText(time);
	}
	
	public void setTest1Int(int count) {
		test1Text.setText("" + count);
	}
	
}
