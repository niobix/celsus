package gui.ai;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JPanel;
import javax.swing.JTextField;

import bundles.BundleManager;

public class Terminal extends JPanel implements ActionListener {
	private static final long serialVersionUID = -4569129430581155477L;
	
	BundleManager bundle;
	
	static TextArea terminal;
	
	JTextField input;
	
	public Terminal(BundleManager bundle) {
		
		this.bundle = bundle;
		
		Font font = new Font("Lucida Sans Typewriter", Font.PLAIN, 18);
		
		GridBagConstraints c = new GridBagConstraints();
		this.setLayout(new GridBagLayout());
		int gridx = 0;
		int gridy = 0;
		
		c.gridx = gridx;
		c.gridy = gridy;
		c.insets = new Insets( 10, 10, 10, 10);
		terminal = new TextArea(9, 100);
		terminal.setFont(font);
		terminal.setBackground(Color.black);
		terminal.setForeground(Color.green);
		this.add(terminal, c);
		
		gridy++;
		
		c.gridy = gridy;
		c.insets = new Insets( 10, 10, 10, 10);
		input = new JTextField(45);
		input.setFont(font);
		input.setBackground(Color.black);
		input.setForeground(Color.green);
		input.addActionListener(this);
		this.add(input, c);
		
		
	}

	@Override
	public void actionPerformed(ActionEvent a) {
		
		try {
			bundle.getAtlas().sendCommand(input.getText());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		input.setText("");
		
	}
	
	public static void addLog(String message) {
		terminal.append(message + "\n");
	}
	
}
