package gui.ai;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JPanel;

import bundles.BundleManager;

public class AIPanel extends JPanel {
	private static final long serialVersionUID = 8720834214142670687L;
	
	BundleManager bundle;
	
	JLabel title;
	
	Terminal terminal;
	
	public AIPanel(BundleManager bundle) {
		
		this.bundle = bundle;
		
		Font font = new Font("Lucida Sans Typewriter", Font.PLAIN, 18);
		
		GridBagConstraints c = new GridBagConstraints();
		this.setLayout(new GridBagLayout());
		int gridx = 0;
		int gridy = 0;
		
		c.gridx = gridx;
		c.gridy = gridy;
		c.insets = new Insets( 10, 10, 10, 10);
		title = new JLabel("Atlas Terminal");
		title.setFont(font);
		this.add(title, c);
		
		gridy++;
		
		c.gridy = gridy;
		c.insets = new Insets( 10, 10, 10, 10);
		c.gridwidth = 3;
		terminal = new Terminal(bundle);
		terminal.setFont(font);
		this.add(terminal, c);
		
		
	}
	
}
