package gui;

import java.awt.GridBagLayout;

import javax.swing.JFrame;

import process.Creator;

public class Frame extends JFrame {
	private static final long serialVersionUID = 4743270606172960944L;

	private JFrame frame = new JFrame();

	public JFrame initialize() {
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
		frame.setSize(1000, 700);
//		frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		frame.setTitle("Celsus Visualization [" + Creator.version + "]");
		frame.setLayout(new GridBagLayout());
		frame.setVisible(true);
		
		return frame;
	}
	
}
