package gui;

import gui.ai.AIPanel;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JFrame;

import bundles.BundleManager;

public class ControlPanel {
	
	JFrame frame;
	BundleManager bundle;

	public ControlPanel(BundleManager bundle) {
		
		this.bundle = bundle;
		
		JFrame frame = new Frame().initialize();
		
		GridBagConstraints c = new GridBagConstraints();
		frame.setLayout(new GridBagLayout());
		int gridx = 0;
		int gridy = 0;
		
		c.gridx = gridx;
		c.gridy = gridy;
		c.insets = new Insets( 10, 10, 10, 10);
		frame.add(new StartStopPanel(bundle.getInputBundle()), c);
		
		gridx++;
		
		c.gridx = gridx;
		frame.add(new DataPanel(bundle.getDataBundle()), c);
		
		gridx++;
		
		c.gridx = gridx;
		frame.add(new SettingsPanel(bundle.getSettingsBundle()), c);
		
		gridy++;
		gridx = 0;
		
		c.gridx = gridx;
		c.gridy = gridy;
		c.gridwidth = 3;
		frame.add(new AIPanel(bundle), c);
		
		frame.revalidate();
		
	}
	
}
