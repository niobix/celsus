package read;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import bundles.DataBundle;
import bundles.InputBundle;

public class BuildProteinPairs {

	DataBundle dataBundle;
	InputBundle inputBundle;
	
	public BuildProteinPairs(DataBundle dataBundle) {
		this.dataBundle = dataBundle;
	}
	
	public void readFile(File fin) throws IOException {
		// Construct BufferedReader from FileReader
		BufferedReader br = new BufferedReader(new FileReader(fin));
		
		int[][] twoLinkArray = new int[4850629][3];
		
		String line = null;
		for (int i = 0;(line = br.readLine()) != null; i++) {
			
			try {
				twoLinkArray[i][0] = Integer.parseInt(line.substring(9, 20));
				twoLinkArray[i][1] = Integer.parseInt(line.substring(30, 41));
				twoLinkArray[i][2] = Integer.parseInt(line.substring(42, 45));
			} catch (NumberFormatException e) {
				
			} catch (StringIndexOutOfBoundsException e) {
				
			}
			
			dataBundle.setTotalPairCount(dataBundle.getTotalPairCount() + 1);
			
			try {
				Thread.sleep(0);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
		}
		
		br.close();
		
		dataBundle.setTwoLinkArray(twoLinkArray);
		
	}
	
}
