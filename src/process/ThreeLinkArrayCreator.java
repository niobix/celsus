package process;

import bundles.DataBundle;

public class ThreeLinkArrayCreator {

	DataBundle dataBundle;
	
	
	
	public ThreeLinkArrayCreator(DataBundle dataBundle) {
		this.dataBundle = dataBundle;
	}
	
	public void generateThreeLinkArray() {
		
		int[][] twoLinkArray = dataBundle.getTwoLinkArray();
		int[] proteinList = dataBundle.getProteinList();
		int threePointCount = 0;
				
		for (int i = 0; i < 20771; i++) { // for each protein
			
			for (int k = 0; k < 4850629; k++) {
				
				if (twoLinkArray[k][0] == proteinList[i]) {
					
					for (int n = k; twoLinkArray[k][0] == twoLinkArray[n][0]; n++) { // for each protein paired with i
						
						if (twoLinkArray[n][1] == proteinList[i]) {
							// TODO add all proteins together into an array
							threePointCount++;
							
						}
						
					}
					
				}
				
			}
				
		}
		
		dataBundle.setTotalThreeLinkCount(threePointCount);
		
	}
	
}
