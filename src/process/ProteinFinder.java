package process;

import java.io.IOException;

import bundles.DataBundle;

public class ProteinFinder {
	
	DataBundle dataBundle;
	
	
	public ProteinFinder(DataBundle dataBundle) {
		this.dataBundle = dataBundle;
	}
	
	public void findProteins() throws IOException {
		// Construct BufferedReader from FileReader
		int[][] twoLinkArray = dataBundle.getTwoLinkArray();
		int[] proteinList = new int[20771];
		
		int count = 0;
		
		for (int i = 0; i < 4850629; i++) {
			
			if ( i > 0 ) {
				if (twoLinkArray[i][0] != twoLinkArray[i-1][0]) {
					count++;
				}
			}
			
			proteinList[count] = twoLinkArray[i][0];
			
		}
		
		dataBundle.setProteinList(proteinList);
		
	}
	
}
