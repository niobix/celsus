package process;

import gui.ControlPanel;

import java.io.File;
import java.io.IOException;

import read.BuildProteinPairs;
import ai.Atlas;
import bundles.BundleManager;
import bundles.DataBundle;
import bundles.InputBundle;
import bundles.SettingsBundle;

public class Creator {
	
	public final static String version = "0.0.1.2 Alpha";

	public static void main(String[] args) throws IOException {
		
		BundleManager bundle = new BundleManager();
		DataBundle dataBundle = new DataBundle();
		InputBundle inputBundle = new InputBundle();
		SettingsBundle settingsBundle = new SettingsBundle();
		Atlas atlas = new Atlas(bundle);
		
		bundle.setAtlas(atlas);
		bundle.setDataBundle(dataBundle);
		bundle.setInputBundle(inputBundle);
		bundle.setSettingsBundle(settingsBundle);
		
		new ControlPanel(bundle);
		
		File linkList = new File("src/resources/9606.protein.links.v9.1.txt");
		
		BuildProteinPairs buildProteinPairs = new BuildProteinPairs(dataBundle);
		ProteinFinder proteinFinder = new ProteinFinder(dataBundle);
		ThreeLinkArrayCreator threeLinkCreator = new ThreeLinkArrayCreator(dataBundle);
		
		// TODO run loop
		for(;;) {
			
			if (inputBundle.isStart()) {
				
				dataBundle.setStartTime();
				dataBundle.clearStopTime();
				
				if (settingsBundle.isTwoPointLink()) {
					dataBundle.setTotalPairCount(0);
					buildProteinPairs.readFile(linkList);
					proteinFinder.findProteins();
				} else if (settingsBundle.isThreePointLink()) {
					threeLinkCreator.generateThreeLinkArray();
				} else if (settingsBundle.isTest1()) {
					
				}
				
				inputBundle.setStart(false);
				dataBundle.setStopTime();
				
			} else {
				
			}
			
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
		}
		
	}

}
