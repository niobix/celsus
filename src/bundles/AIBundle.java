package bundles;

import ai.Patient;

public class AIBundle {

	Patient patients[];
	String patientNames[];
	
	public Patient[] getPatients() {
		return patients;
	}
	public void setPatients(Patient[] patients) {
		this.patients = patients;
	}
	public String[] getPatientNames() {
		return patientNames;
	}
	public void setPatientNames(String[] patientNames) {
		this.patientNames = patientNames;
	}
	
}
