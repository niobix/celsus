package bundles;

import gui.DataPanel;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DataBundle {
	
	private int triangles;
	private DataPanel dataPanel;
	private int totalPairCount = 0;
	private int totalThreeLinkCount = 0;
	private String startTime = "0";
	private String stopTime = "0";
	private int[][] twoLinkArray = new int[4850629][3];
	private int[] proteinList = new int[20771];
	private int testInt1;

	public int getTriangles() {
		return triangles;
	}

	public void setTriangles(int triangles) {
		this.triangles = triangles;
	}

	public DataPanel getDataPanel() {
		return dataPanel;
	}

	public void setDataPanel(DataPanel dataPanel) {
		this.dataPanel = dataPanel;
	}

	public int getTotalPairCount() {
		return totalPairCount;
	}

	public void setTotalPairCount(int totalPairCount) {
		this.totalPairCount = totalPairCount;
		dataPanel.setTotalPairCount(this.totalPairCount);
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime() {
		this.startTime = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
		dataPanel.setStartTime(startTime);
	}

	public String getStopTime() {
		return stopTime;
	}

	public void setStopTime() {
		this.stopTime = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
		dataPanel.setStopTime(stopTime);
	}

	public void clearStopTime() {
		dataPanel.setStopTime("0");
	}

	public int[][] getTwoLinkArray() {
		return twoLinkArray;
	}

	public void setTwoLinkArray(int[][] twoLinkArray) {
		this.twoLinkArray = twoLinkArray;
	}

	public int getTotalThreeLinkCount() {
		return totalThreeLinkCount;
	}

	public void setTotalThreeLinkCount(int totalThreeLinkCount) {
		this.totalThreeLinkCount = totalThreeLinkCount;
		dataPanel.setTotalThreeLinkCount(this.totalThreeLinkCount);
	}

	public int getTestInt1() {
		return testInt1;
	}

	public void setTestInt1(int testInt1) {
		this.testInt1 = testInt1;
		dataPanel.setTest1Int(testInt1);
	}

	public int[] getProteinList() {
		return proteinList;
	}

	public void setProteinList(int[] proteinList) {
		this.proteinList = proteinList;
	}
	
}
