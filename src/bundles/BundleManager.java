package bundles;

import ai.Atlas;

public class BundleManager {

	DataBundle dataBundle;
	InputBundle inputBundle;
	SettingsBundle settingsBundle;
	AIBundle aIBundle;
	Atlas atlas;

	public DataBundle getDataBundle() {
		return dataBundle;
	}

	public void setDataBundle(DataBundle dataBundle) {
		this.dataBundle = dataBundle;
	}

	public InputBundle getInputBundle() {
		return inputBundle;
	}

	public void setInputBundle(InputBundle inputBundle) {
		this.inputBundle = inputBundle;
	}

	public SettingsBundle getSettingsBundle() {
		return settingsBundle;
	}

	public void setSettingsBundle(SettingsBundle settingsBundle) {
		this.settingsBundle = settingsBundle;
	}

	public Atlas getAtlas() {
		return atlas;
	}

	public void setAtlas(Atlas atlas) {
		this.atlas = atlas;
	}

	public AIBundle getaIBundle() {
		return aIBundle;
	}

	public void setaIBundle(AIBundle aIBundle) {
		this.aIBundle = aIBundle;
	}
	
}
