package bundles;

public class SettingsBundle {

	private boolean twoPointLink = true;
	private boolean threePointLink = false;
	private boolean fourPointLink = false;
	private boolean test1 = false;

	public boolean isTwoPointLink() {
		return twoPointLink;
	}

	public void setTwoPointLink(boolean twoPointLink) {
		setAllToFalse(twoPointLink);
		this.twoPointLink = twoPointLink;
	}

	public boolean isThreePointLink() {
		return threePointLink;
	}

	public void setThreePointLink(boolean threePointLink) {
		setAllToFalse(threePointLink);
		this.threePointLink = threePointLink;
	}

	public boolean isFourPointLink() {
		return fourPointLink;
	}

	public void setFourPointLink(boolean fourPointLink) {
		setAllToFalse(fourPointLink);
		this.fourPointLink = fourPointLink;
	}

	public boolean isTest1() {
		return test1;
	}

	public void setTest1(boolean test1) {
		setAllToFalse(test1);
		this.test1 = test1;
	}
	
	public void setAllToFalse(boolean in) {
		if (in == true) {
			twoPointLink = false;
			threePointLink = false;
			fourPointLink = false;
			test1 = false;
		}
	}
	
}
