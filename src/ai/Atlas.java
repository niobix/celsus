package ai;

import gui.ai.Terminal;

import java.io.IOException;

import bundles.AIBundle;
import bundles.BundleManager;

public class Atlas {

	CreateCommands create;
	CountCommands count;
	GetCommands get;
	
	public Atlas (BundleManager bundle) {
		
		bundle.setaIBundle(new AIBundle());
		
		create = new CreateCommands(bundle.getaIBundle());
		count = new CountCommands(bundle.getaIBundle());
		get = new GetCommands(bundle.getaIBundle());
		
	}
	
	public void sendCommand(String command) throws IOException {
		
		String parts[] = command.split(" ");
		String prefix = parts[0];
		
		if (prefix.equals("create")) {
			create.passCommand(parts);
		} else if (prefix.equals("count")) {
			count.passCommand(parts);
		} else if (prefix.equals("get")) {
			get.passCommand(parts);
		} else {
			Terminal.addLog("Prefix not found");
		}
		
	}
	
}
