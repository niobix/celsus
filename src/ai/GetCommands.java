package ai;

import gui.ai.Terminal;

import java.io.IOException;

import bundles.AIBundle;

public class GetCommands {

	private AIBundle aIBundle;

	public GetCommands(AIBundle aIBundle) {
		this.aIBundle = aIBundle;
	}
	
	public void passCommand(String parts[]) throws IOException {
		
		if (parts[1].equals("patient")) {
			getPatient(parts[2]);
		} 
		
	}
	
	private void getPatient(String index) {
		
		Terminal.addLog("Patient: " + aIBundle.getPatients()[Integer.parseInt(index)].getName()
				+ "\nMutations: " + aIBundle.getPatients()[Integer.parseInt(index)].getMutatedGeneList());
		
	}
	
}
