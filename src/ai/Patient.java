package ai;

import java.util.ArrayList;

public class Patient {
	
	private String name;
	private ArrayList<String> mutatedGeneList = new ArrayList<String>();
	private int numberOfMutations;
	private int numberOfPotential;

	public Patient(String name) {
		this.name = name;
	}
	
	public Patient(String name, int numberOfMutations, int numberOfPotential) {
		this.name = name;
		this.numberOfMutations = numberOfMutations;
		this.numberOfPotential = numberOfPotential;
	}
	
	public Patient(String name, ArrayList<String> mutatedGeneList, int numberOfMutations, int numberOfPotential) {
		this.name = name;
		this.mutatedGeneList = mutatedGeneList;
		this.numberOfMutations = numberOfMutations;
		this.numberOfPotential = numberOfPotential;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<String> getMutatedGeneList() {
		return mutatedGeneList;
	}

	public void setMutatedGeneList(ArrayList<String> mutatedGeneList) {
		this.mutatedGeneList = mutatedGeneList;
	}
	
	public void addMutatedGene(String gene) {
		mutatedGeneList.add(gene);
	}

	public int getNumberOfMutations() {
		return numberOfMutations;
	}

	public void setNumberOfMutations(int numberOfMutations) {
		this.numberOfMutations = numberOfMutations;
	}

	public int getNumberOfPotential() {
		return numberOfPotential;
	}

	public void setNumberOfPotential(int numberOfPotential) {
		this.numberOfPotential = numberOfPotential;
	}
	
}
