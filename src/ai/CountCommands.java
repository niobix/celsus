package ai;

import gui.ai.Terminal;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import bundles.AIBundle;

public class CountCommands {
	
	File patientFile = new File("src/resources/LUSC_results_clinical.tsv");
	AIBundle aIBundle;
	
	public CountCommands (	AIBundle aIBundle) {
		this.aIBundle = aIBundle;
	}

	public void passCommand(String parts[]) throws IOException {
		
		if (parts[1].equals("patients")) {
			Terminal.addLog("Counting Patients");
			countPatients();
		}
		
	}
	
	public void countPatients() throws IOException {
		
		ArrayList<String> nameList = new ArrayList<String>();
		int count = 0;
		
		BufferedReader br = new BufferedReader(new FileReader(patientFile));
		
		String line = null;
		for (@SuppressWarnings("unused")
		int i = 0;(line = br.readLine()) != null; i++) {
			
//			System.out.println(line);
			
			String parts[] = line.split("	");
			
			if (!nameList.contains(parts[0])) {
				nameList.add(parts[0]);
				count++;
			}
			
		}
		
		br.close();
		
		Terminal.addLog(count + " Patients");
		
	}
	
}
