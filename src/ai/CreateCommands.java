package ai;

import gui.ai.Terminal;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import bundles.AIBundle;

public class CreateCommands {

	private File patientFile = new File("src/resources/LUSC_results_clinical.tsv");
	private AIBundle aIBundle;
	
	int count = 0;
	
	public CreateCommands(AIBundle aIBundle) {
		this.aIBundle = aIBundle;
	}
	
	public void passCommand(String parts[]) throws IOException {
		
		if (parts[1].equals("patients")) {
			Terminal.addLog("Creating Patients");
			createPatients();
		} 
		
	}
	
	private void createPatients() throws IOException {
		
		BufferedReader br = new BufferedReader(new FileReader(patientFile));
				
		int patientCount = 176;
		
		Patient patients[] = new Patient[patientCount];
		String patientNames[] = new String[patientCount];
		
		String line = null;
		
		for (@SuppressWarnings("unused")
		int i = 0;(line = br.readLine()) != null; i++) {
			
			String parts[] = line.split("	");
			
			if (!contains(patientNames, parts[0])) {
				addPatient(parts[0], patientNames, patients, parts);
			} else {
				getPatient(patients, parts[0]).addMutatedGene(parts[1]);
			}
		}
		
		br.close();
		
		aIBundle.setPatientNames(patientNames);
		aIBundle.setPatients(patients);
		
		Terminal.addLog("Created " + count + " patients");
	}
	
	private boolean contains(String list[], String name) {
		
		for (int i = 0; i < list.length; i++) {
			if (list[i] != null) {
				if (list[i].equals(name)) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	private void addPatient(String name, String patientNames[], Patient patients[], String parts[]) {
		
		for (int i = 0; i < patientNames.length; i++) {
			
			if (patientNames[i] == null) {
				patientNames[i] = name;
				patients[i] = new Patient(name);
				count++;
				
				// add patient details
				patients[i].addMutatedGene(parts[1]);
				
				i = patientNames.length;
			}
			
		}
		
	}
	
	private Patient getPatient(Patient list[], String name) {
		
		for (int i = 0; i < list.length; i++) {
			if (list[i] != null) {
				if (list[i].getName().equals(name)) {
					return list[i];
				}
			}
		}
		
		return list[1];
		
	}
	
}
